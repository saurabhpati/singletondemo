using System;

namespace SingletonDemo
{
    public static class ProgramConfig 
    {
        [Flags]
        public enum RuntimeConfig
        {
            Lousy = 1,
            ThreadSafe = 2,
            VolatileRef = 4,
            StaticLazy = 8,
            FullLazyThreadSafe = 16,
            FullLazyThreadUnsafe = 32
        } 
    }
}