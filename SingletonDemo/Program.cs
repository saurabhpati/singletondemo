﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SingletonDemo.Service;
using static SingletonDemo.ProgramConfig;

namespace SingletonDemo
{
    class Program
    {
        private static RuntimeConfig Config { get; set; }

        /// <summary>
        /// Recommend to run one singleton at a time to view better results.
        /// </summary>
        static Program()
        {
            Config = RuntimeConfig.FullLazyThreadUnsafe;
        }

        static void Main(string[] args)
        {
            List<Action> demoActions = new List<Action>();

            if (Config.HasFlag(RuntimeConfig.Lousy))
            {
                demoActions.Add(RunLousySingletonDemo);
            }
            if (Config.HasFlag(RuntimeConfig.ThreadSafe))
            {
                demoActions.Add(RunThreadSafeSingletonDemo);
            }
            if (Config.HasFlag(RuntimeConfig.VolatileRef))
            {
                demoActions.Add(RunVolatileRefSingletonDemo);
            }
            if (Config.HasFlag(RuntimeConfig.StaticLazy))
            {
                demoActions.Add(RunStaticLazySingletonDemo);
            }
            if (Config.HasFlag(RuntimeConfig.FullLazyThreadSafe))
            {
                demoActions.Add(RunFullLazyThreadSafeSingletonDemo);
            }
            if (Config.HasFlag(RuntimeConfig.FullLazyThreadUnsafe))
            {
                demoActions.Add(RunFullLazyThreadUnsafeSingletonDemo);
            }

            demoActions.ForEach(demo => demo());
            Console.WriteLine("---------PRESS ANY KEY TO EXIT---------");
            Console.ReadKey();
        }

        private static void RunLousySingletonDemo()
        {
            LousySingletonService instance1 = null;
            LousySingletonService instance2 = null;
            Console.WriteLine("-------- LOUSY SINGLETON DEMO ---------");

            Task.Factory.StartNew(() =>
            {
                instance1 = LousySingletonService.Instance;
                Console.WriteLine($"instance 1's number {instance1.InstanceNumber}");
            });

            Task.Factory.StartNew(() =>
            {
                instance2 = LousySingletonService.Instance;
                Console.WriteLine($"instance 2's number {instance2.InstanceNumber}");
            });
        }

        private static void RunThreadSafeSingletonDemo()
        {
            ThreadSafeSingletonService instance1 = null;
            ThreadSafeSingletonService instance2 = null;
            Console.WriteLine("-------- THREAD SAFE SINGLETON DEMO ---------");

            Task.Factory.StartNew(() =>
            {
                instance1 = ThreadSafeSingletonService.Instance;
                Console.WriteLine($"instance 1's number {instance1.InstanceNumber}");
            });

            Task.Factory.StartNew(() =>
            {
                instance2 = ThreadSafeSingletonService.Instance;
                Console.WriteLine($"instance 2's number {instance2.InstanceNumber}");
            });
        }

        private static void RunVolatileRefSingletonDemo()
        {
            VolatileRefSingletonService instance1 = null;
            VolatileRefSingletonService instance2 = null;
            Console.WriteLine("-------- VOLATILE REF SINGLETON DEMO ---------");

            Task.Factory.StartNew(() =>
            {
                instance1 = VolatileRefSingletonService.Instance;
                Console.WriteLine($"instance 1's number {instance1.InstanceNumber}");
            });

            Task.Factory.StartNew(() =>
            {
                instance2 = VolatileRefSingletonService.Instance;
                Console.WriteLine($"instance 2's number {instance2.InstanceNumber}");
            });
        }

        private static void RunStaticLazySingletonDemo()
        {
            StaticLazySingletonService instance1 = null;
            StaticLazySingletonService instance2 = null;
            Console.WriteLine("-------- STATIC LAZY SINGLETON DEMO ---------");

            Task.Factory.StartNew(() =>
            {
                instance1 = StaticLazySingletonService.Instance;
                Console.WriteLine($"instance 1's number {instance1.InstanceNumber}");
            });

            Task.Factory.StartNew(() =>
            {
                instance2 = StaticLazySingletonService.Instance;
                Console.WriteLine($"instance 2's number {instance2.InstanceNumber}");
            });
        }

        private static void RunFullLazyThreadSafeSingletonDemo()
        {
            FullLazySingletonService instance1 = null;
            FullLazySingletonService instance2 = null;
            Console.WriteLine("-------- FULL LAZY THREAD SAFE SINGLETON DEMO ---------");

            Task.Factory.StartNew(() =>
            {
                instance1 = FullLazySingletonService.ThreadSafeInstance;
                Console.WriteLine($"instance 1's number {instance1.InstanceNumber}");
            });

            Task.Factory.StartNew(() =>
            {
                instance2 = FullLazySingletonService.ThreadSafeInstance;
                Console.WriteLine($"instance 2's number {instance2.InstanceNumber}");
            });
        }

        private static void RunFullLazyThreadUnsafeSingletonDemo()
        {
            FullLazySingletonService instance1 = null;
            FullLazySingletonService instance2 = null;
            Console.WriteLine("-------- FULL LAZY THREAD UNSAFE SINGLETON DEMO ---------");

            Task.Factory.StartNew(() =>
            {
                instance1 = FullLazySingletonService.ThreadUnsafeInstance;
                Console.WriteLine($"instance 1's number {instance1.InstanceNumber}");
            });

            Task.Factory.StartNew(() =>
            {
                instance2 = FullLazySingletonService.ThreadUnsafeInstance;
                Console.WriteLine($"instance 2's number {instance2.InstanceNumber}");
            });
        }
    }
}
