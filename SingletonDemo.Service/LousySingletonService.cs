﻿using System;

namespace SingletonDemo.Service
{
    /// <summary>
    /// This is lousy because it is not thread-safe.
    /// It is a general practice to keep the Singleton service sealed to avoid subclassing although it doesn't make 
    /// a big difference because the constructor is private.
    /// </summary>
    public sealed class LousySingletonService
    {
        private static LousySingletonService instance = null;
        private static int counter = 0;

        private LousySingletonService()
        {
        }

        public int InstanceNumber { get; set; }

        public static LousySingletonService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LousySingletonService() { InstanceNumber = ++counter };
                }

                return instance;
            }
        }
    }
}
