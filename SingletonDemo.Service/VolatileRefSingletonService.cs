﻿using System;

namespace SingletonDemo.Service
{
    public sealed class VolatileRefSingletonService
    {
        private static volatile VolatileRefSingletonService instance = null;
        private static int counter = 0;
        private static readonly object lockObj = new Object();

        private VolatileRefSingletonService()
        {
        }

        public int InstanceNumber { get; set; }

        public static VolatileRefSingletonService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (lockObj)
                    {
                        if (instance == null)
                        {
                            instance = new VolatileRefSingletonService() { InstanceNumber = ++counter };
                        }
                    }
                }

                return instance;
            }
        }
    }
}
