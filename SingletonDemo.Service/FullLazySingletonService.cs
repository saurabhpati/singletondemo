﻿using System;

namespace SingletonDemo.Service
{
    public class FullLazySingletonService
    {
        private static readonly Lazy<FullLazySingletonService> threadSafeInstance = new Lazy<FullLazySingletonService>(() => new FullLazySingletonService() { InstanceNumber = ++counter }, true);
        private static readonly Lazy<FullLazySingletonService> threadUnsafeInstance = new Lazy<FullLazySingletonService>(() => new FullLazySingletonService() { InstanceNumber = ++counter }, false);
        private static int counter = 0;

        private FullLazySingletonService()
        {
        }

        public int InstanceNumber { get; set; }

        public static FullLazySingletonService ThreadSafeInstance { get; } = threadSafeInstance.Value;

        public static FullLazySingletonService ThreadUnsafeInstance { get; } = threadUnsafeInstance.Value;
    }
}
