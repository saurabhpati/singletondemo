﻿using System;

namespace SingletonDemo.Service
{
    public sealed class ThreadSafeSingletonService
    {
        private static ThreadSafeSingletonService instance = null;
        private static int counter = 0;
        private static readonly object lockObj = new Object();

        private ThreadSafeSingletonService()
        {
        }

        public int InstanceNumber { get; set; }

        public static ThreadSafeSingletonService Instance
        {
            get
            {
                lock (lockObj)
                {
                    if (instance == null)
                    {
                        instance = new ThreadSafeSingletonService() { InstanceNumber = ++counter };
                    }

                    return instance;
                }
            }
        }
    }
}
