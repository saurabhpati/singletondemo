﻿namespace SingletonDemo.Service
{
    public class StaticLazySingletonService
    {
        private static int counter = 0;

        static StaticLazySingletonService()
        {
        }

        private StaticLazySingletonService()
        {
        }

        public int InstanceNumber { get; set; }

        public static StaticLazySingletonService Instance { get; } = new StaticLazySingletonService() { InstanceNumber = ++counter };
    }
}
